DAY 1
-----------
Refreshment
---------------
- Dev. flow
 - in. Laravel

3 teknik
----------
1. MPA
 - CDN, inject layout
 - download ke local server

2. SPA, Full stack dev.
 - Back end + Front end
 - semua code dlm satu projek

3. SPA, 2 sub proj.
 - front-end sub pro.
    vuejs proj
 - vue create <nama projek>

 - back-end sub pro.
   laravel proj
 - composer crate-project laravel/laravel <nama projek>


 - sakila for MySQL
 - MySQL workbench
 - JSON placeholder

CSS Framework
---------------
- twitter Bootstrap
  getbootstrap.com
- bulma
- foundation
- materialize



Production
------------
> npm run build
 - compile "dist"

Git Lab / Git Hub / Bit Bucket
 - automation (Jenkin)
 - npm run build
 - CI (Continous Integration) / CD (Continous Dev.)




Front End App
----------------
- component
- TypeScript
- installation
 - nodejs
 - npm
 - ng generate component <nama>
   - .spec.ts, .ts, .css, .html
   - directives
    [(ngModel)] - two way binding
    *ngIf
    *ngFor
    [attribute] - binding
    (event)
   - <app-hello></app-hello>


gitlab.com/azman1204/angular-day2

1. Bootstrap vs Materialize

2. Konsep Module
 - admin
 - public
 - registration 

3. Routing
 - switch page mcm MPA
 - SPA switch component
 - <router-outlet>


Http Request
 - read data / insert / update / delete
 - JSON server

Service
 - request
 - 

Typescript

let, const, class, intrface, ... constructor(?, )

Interface
 

Class, Interface

class Person {

}


Interface Person {
  nama: string,
  age: number
}


DAY 3
------
1. typescript
 - class
 - interface
 - constructor
 - enum
 - type: any, string, number, boolean, Function, Interface/Object
 - let / const

2. Service
 - query data (CRUD)
 - http
 - injection (inject ke component)
 - mcm model
 - data, declare data, interface, class

npm install -g typescript
tsc --version
tsc <nama file.ts>
- compile ke .js
trc -w
 w = watch

ext vsc
-------
-live server


js
--
Promise vs Observable



async(parallel) vs sync(sequence)

ng generate service service/user
ng generate interface interface/user


